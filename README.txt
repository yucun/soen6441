SOEN 6441/4 WW

Group #:3

Group members:
Cheng Xin
Kong Dehui
Li Yucun
Liu Zhenjiang

File Description in the archive:
Data Folder: Contain the initial game data as well as the saved game status.
Design Folder: Contain the basic UI design in JPG as well as VISIO file.
dist Folder: Contain the generated JavaDoc.
nbproject Folder: Contain the project file.
Pic Folder: Contain the picture used in the game.
src Folder: Contain the source code.
	src/Controller: Contain all the control function including initial game, save game, load game...
	src/Model: Contain all the classes including the area class, card class, coin class, piece class, player class.
	src/Resource: Contain all the area card picture.
	src/View: Contain the main function including the MainFrame.
test Folder: Contain all the test files.

Online documents URL: ../DiscWord/dist/javadoc/index.html

Running instructions:
1.Run the MainFrame.java in ../DiscWord/src/View.
2.Input all the players name in the text area, bottom left of the MainFrame.
3.Press the Init button to initial the game.
4.Press the player card button can see all the green and brown player cards.
5.Press the random event card button can see all the random event cards.
6.Press the personality card button can see all the personality cards.
7.Press the city area card button can see all the city area cards.
8.Input the save file name and press the save button, all the game information will be saved in the ../DiscWorld/Data/statusData folder.
9.Input the game status file name in the text area between the load button, you can load the status file from the CSV file to the panel.
10.On the upper left of the panel can check all the map information including the rest Demon number, rest Troll number and rest Trouble marker number.
11.On the bottom right of the panel, you can check the current money amount  in bank and who is the current player.
