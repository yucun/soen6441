package Controller;

import Model.piece.MinionPiece;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author yucunli
 */
public class Utils {

    /**
     *Convert a list of integers into a string.
     * @param list
     * @return String
     */
    public static String listToString(List<Integer> list){
        String result = "";
        for(Integer integer : list){
            result += integer + " ";
        }
        return result;
    }
    
    // input : area's minion List

    /**
     *Convert a minion list into a string.
     * @param list
     * @param minionMap
     * @return String
     */
        public static String minionListToColorString(List<Integer> list, HashMap<Integer, MinionPiece> minionMap){
        String result = "";
        for(Integer integer : list){
            result += minionMap.get(integer).getColor().toString() + " ";
        }
        
        return result;
    }
}
