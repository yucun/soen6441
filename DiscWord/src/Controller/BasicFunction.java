

package Controller;

import Model.Color;
import Model.DiscWorld;
import Model.area.Area;
import Model.card.BrownPlayerCard;
import Model.card.CityAreaCard;
import Model.card.GreenPlayerCard;
import Model.card.PersonalityCard;
import Model.card.PlayerCard;
import Model.card.RandomEventCard;
import Model.coin.Coin;
import Model.coin.GoldCoin;
import Model.coin.SilverCoin;
import Model.piece.BuildingPiece;
import Model.piece.DemonPiece;
import Model.piece.MinionPiece;
import Model.piece.TrollPiece;
import Model.piece.TroubleMarker;
import Model.player.Player;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author yucunli
 */
public class BasicFunction {
    
    /**
     *Number of silver coin: 35.
     */
    public static final int NUM_SILVER_COIN = 35;

    /**
     *Number of gold coin: 17.
     */
    public static final int NUM_GOLD_COIN = 17;
    
    /**
     *Number of area: 12.
     */
    public static final int NUM_AREA = 12;

    /**
     *number of green player card: 48.
     */
    public static final int NUM_GREEN_PLAYER_CARD = 48;

    /**
     *Number of brown player card: 53.
     */
    public static final int NUM_BROWN_PLAYER_CARD = 53;

    /**
     *Number of city area card: 12.
     */
    public static final int NUM_CITY_AREA_CARD = 12;

    /**
     *Number of personality card: 7.
     */
    public static final int NUM_PERSONALITY_CARD = 7;

    /**
     *Number of random event card: 12.
     */
    public static final int NUM_RANDOM_EVENT_CARD = 12;
    
    /**
     *Number of building piece for each player: 6.
     */
    public static final int NUM_OF_BUILDING_PIECE_SET = 6;

    /**
     *Number of minion piece for each player: 12.
     */
    public static final int NUM_OF_MINION_PIECE_SET = 12;

    /**
     *Number of all the demons piece: 4.
     */
    public static final int NUM_DEMON_PIECE = 4;

    /**
     *Number of all the troll piece: 3.
     */
    public static final int NUM_TROLL_PIECE = 3;

    /**
     *Number of all the trouble marker: 12.
     */
    public static final int NUM_TROUBLE_MARKER = 12;
    
    /**
     *Default player personality card ID: -1.
     */
    public static final int DEFAULT_PLAYER_PERSONALITYCARD = -1;

    /**
     *Differentiation ID between gold coin and silver coin.
     */
    public static final int SILVER_GOLD_COIN_ID_DIFF = 100;

    /**
     *Differentiation ID between green and brown cards.
     */
    public static final int GREEN_BROWN_PLAYER_CARD_DIFF = 100;

    /**
     *Differentiation ID between building piece.
     */
    public static final int BUILDING_PIECE_ID_DIFF = 10;

    /**
     *Differentiation ID between minion piece.
     */
    public static final int MINION_PIECE_ID_DIFF = 20;
    
    /**
     *Status data file directory.
     */
    public static final String STATUS_DATA_FILE_DIR = "./Data/statusData/";

    /**
     *Initial data file directory.
     */
    public static final String INIT_DATA_FILE_DIR = "./Data/initData/";
    
    /**
     *Default first player ID: 0.
     */
    public static final int DEFAULT_FIRST_PLAYER_ID = 0;

    /**
     *Default undefined player ID: -1.
     */
    public static final int DEFAULT_UNDEFINED_PLAYID = -1;

    /**
     *Default area piece number: -1.
     */
    public static final int DEFAULT_PIECE_AREA_NUMBER = -1;
    
    /**
     *Shadow Area ID: 7.
     */
    public static final int THE_SHADOW_AREA_NUM = 7;

    /**
     *Scours Area ID: 5.
     */
    public static final int THE_SCOURS_AREA_NUM = 5;

    /**
     *Dolly Sister Area ID: 1.
     */
    public static final int THE_DOLLY_SISTERS_AREA_NUM = 1;
    
    /**
     *Default number of gold coin for each player: 2.
     */
    public static final int DEFAULT_NUM_GOLD_COIN_FOR_PLAYER = 2;

    /**
     *Default number of player card for each player: 5.
     */
    public static final int DEFAULT_NUM_PLAYER_CARD_FOR_PLAYER = 5;
    
    private int numPlayer;
    
    private DiscWorld discWorld;
    
    /**
     *Function to load CSV file.
     * @param filename
     */
    public void load(String filename){
        discWorld = new DiscWorld();
        
        LoadCSV loadCSV = new LoadCSV(INIT_DATA_FILE_DIR);
        
        loadCSV.loadArea(discWorld.getArea_HASH());
        
        loadCSV = new LoadCSV(STATUS_DATA_FILE_DIR+filename+"/");
        
        loadCSV.loadCityAreaCard(discWorld.getCityAreaCard_HASH());
        loadCSV.loadPersonalityCard(discWorld.getPersonalityCard_HASH());
        loadCSV.loadPlayerCard(discWorld.getPlayerCard_HASH());
        loadCSV.loadRandomEventCard(discWorld.getRandomEventCard_HASH());
        
        loadCSV.loadBuildingPiece(discWorld.getBuildingPiece_HASH());
        loadCSV.loadMinionPiece(discWorld.getMinionPiece_HASH());
        loadCSV.loadDemonPiece(discWorld.getDemonPiece_HASH());
        loadCSV.loadTrollPiece(discWorld.getTrollPiece_HASH());
        loadCSV.loadTroubleMarker(discWorld.getTroubleMarker_HASH());
        
        loadCSV.loadPlayer(discWorld.getPlayer_HASH());
        
        loadCSV.loadCoin(discWorld.getCoin_HASH());
        
        this.numPlayer = discWorld.getPlayer_HASH().size();
        // decide cur_player
        for(Player player : discWorld.getPlayer_HASH().values()){
            if(player.getOrder() == 0){
                discWorld.setCur_Player(player.getID());
            }
        }
        // associate PersonalityCard to player
        for(PersonalityCard personalityCard : discWorld.getPersonalityCard_HASH().values()){
            if(personalityCard.getPlayerID() != DEFAULT_UNDEFINED_PLAYID){
                discWorld.getPlayer_HASH().get(personalityCard.getPlayerID()).
                        setPersonalityCardID(personalityCard.getId());
            }
        }
        // associate CityAreaCard to player
        for(CityAreaCard cityAreaCard : discWorld.getCityAreaCard_HASH().values()){
            if(cityAreaCard.getPlayerID() != DEFAULT_UNDEFINED_PLAYID) {
                discWorld.getPlayer_HASH().get(cityAreaCard.getPlayerID()).getCityAreaCardList().add(cityAreaCard.getId());
            }
        }
        // associate PlayerCard to player
        for(PlayerCard playerCard : discWorld.getPlayerCard_HASH().values()){
            if(playerCard.getPlayerID() != DEFAULT_UNDEFINED_PLAYID) {
                discWorld.getPlayer_HASH().get(playerCard.getPlayerID()).getPlayerCardList().add(playerCard.getId());
            }
        }
        // associate RandomEventCard to player
        for(RandomEventCard randomEventCard : discWorld.getRandomEventCard_HASH().values()) {
            if(randomEventCard.getPlayerID() != DEFAULT_UNDEFINED_PLAYID) {
                discWorld.getPlayer_HASH().get(randomEventCard.getPlayerID()).getRandomEventCardList().add(randomEventCard.getId());
            }
        }
        // associate Coin to player
        for(Coin coin : discWorld.getCoin_HASH().values()){
            if(coin.getPlayerID() != DEFAULT_UNDEFINED_PLAYID){
                discWorld.getPlayer_HASH().get(coin.getPlayerID()).getCoinList().add(coin.getId());
            }
        }
        // associate Building Piece to player and Area
        for(BuildingPiece buildingPiece : discWorld.getBuildingPiece_HASH().values()){
            discWorld.getPlayer_HASH().get(buildingPiece.getPlayerID()).getBuildingList().add(buildingPiece.getId());
            if(buildingPiece.getAreaNumber() != DEFAULT_PIECE_AREA_NUMBER){
                discWorld.getArea_HASH().get(buildingPiece.getAreaNumber()).setBuildingNum(buildingPiece.getId());
            }
        }
        // associate Minion Piece to player and Area
        for(MinionPiece minionPiece : discWorld.getMinionPiece_HASH().values()){
            discWorld.getPlayer_HASH().get(minionPiece.getPlayerID()).getMinionList().add(minionPiece.getId());
            if(minionPiece.getAreaNumber() != DEFAULT_PIECE_AREA_NUMBER){
                discWorld.getArea_HASH().get(minionPiece.getAreaNumber()).getMinionList().add(minionPiece.getId());
            }
        }
        // associate Demon Piece to Area
        for(DemonPiece demonPiece : discWorld.getDemonPiece_HASH().values()){
            if(demonPiece.getAreaNumber() != DEFAULT_PIECE_AREA_NUMBER) {
                discWorld.getArea_HASH().get(demonPiece.getAreaNumber()).getDemonList().add(demonPiece.getId());
            }
        }
        // associate Troll Piece to Area
        for(TrollPiece trollPiece : discWorld.getTrollPiece_HASH().values()){
            if(trollPiece.getAreaNumber() != DEFAULT_PIECE_AREA_NUMBER) {
                discWorld.getArea_HASH().get(trollPiece.getAreaNumber()).getTrollList().add(trollPiece.getId());
            }
        }
        // associate TroubleMarker to Area
        for(TroubleMarker troubleMarker : discWorld.getTroubleMarker_HASH().values()){
            if(troubleMarker.getAreaNumber() != DEFAULT_PIECE_AREA_NUMBER) {
                discWorld.getArea_HASH().get(troubleMarker.getAreaNumber()).setTroubleMarkerNum(troubleMarker.getId());
            }
        }
        
        // associate Coin to Player
        for(Coin coin : discWorld.getCoin_HASH().values()){
            if(coin.getPlayerID() != DEFAULT_UNDEFINED_PLAYID){
                discWorld.getPlayer_HASH().get(coin.getPlayerID()).getCoinList().add(coin.getId());
            }
        }
        
    }
    
    /**
     *Function to save CSV file.
     * @param filename
     */
    public void save(String filename){
        SaveCSV saveCSV = new SaveCSV(STATUS_DATA_FILE_DIR+filename+"/");
        
        saveCSV.savePlayer(discWorld.getPlayer_HASH());
        
        saveCSV.saveCityAreaCard(discWorld.getCityAreaCard_HASH());
        saveCSV.savePersonalityCard(discWorld.getPersonalityCard_HASH());
        saveCSV.saveRandomEventCard(discWorld.getRandomEventCard_HASH());
        saveCSV.savePlayerCard(discWorld.getPlayerCard_HASH());
        
        saveCSV.saveCoin(discWorld.getCoin_HASH());
        
        saveCSV.saveBuildingPiece(discWorld.getBuildingPiece_HASH());
        saveCSV.saveMinionPiece(discWorld.getMinionPiece_HASH());
        saveCSV.saveDemonPiece(discWorld.getDemonPiece_HASH());
        saveCSV.saveTrollPiece(discWorld.getTrollPiece_HASH());
        saveCSV.saveTroubleMarker(discWorld.getTroubleMarker_HASH());
    }
    
    /**
     *Function to initial the game and all the data.
     * @param player
     * @param colorArray
     */
    public void init(String[] player, Color[] colorArray){
        discWorld = new DiscWorld();
        this.numPlayer = player.length;
        discWorld.setCur_Player(DEFAULT_FIRST_PLAYER_ID);
        
        LoadCSV loadCSV = new LoadCSV(INIT_DATA_FILE_DIR);
        
        loadCSV.loadArea(discWorld.getArea_HASH());
        loadCSV.loadCityAreaCard(discWorld.getCityAreaCard_HASH());
        loadCSV.loadPersonalityCard(discWorld.getPersonalityCard_HASH());
        loadCSV.loadRandomEventCard(discWorld.getRandomEventCard_HASH());
        
        for(int count = 0; count < NUM_GREEN_PLAYER_CARD; count++){
            PlayerCard playerCard = new GreenPlayerCard(count, PlayerCard.GREEN, "action", DEFAULT_UNDEFINED_PLAYID);
            discWorld.getPlayerCard_HASH().put(playerCard.getId(), playerCard);
        }
        for(int count = 0; count < NUM_BROWN_PLAYER_CARD; count++){
            PlayerCard playerCard = new BrownPlayerCard(GREEN_BROWN_PLAYER_CARD_DIFF+count, PlayerCard.Brown, "action", DEFAULT_UNDEFINED_PLAYID);
            discWorld.getPlayerCard_HASH().put(playerCard.getId(), playerCard);
        }
        
        for(int count = 0; count < NUM_SILVER_COIN; count++){
            discWorld.getCoin_HASH().put(count, new SilverCoin(count, DEFAULT_UNDEFINED_PLAYID));
        }
        for(int count = 0; count < NUM_GOLD_COIN; count++){
            discWorld.getCoin_HASH().put(count + SILVER_GOLD_COIN_ID_DIFF, new GoldCoin(count + SILVER_GOLD_COIN_ID_DIFF, DEFAULT_UNDEFINED_PLAYID));
        }
        
        // init Player, Building Piece, Minion Piece
        for(int index_of_player = 0; index_of_player < player.length; index_of_player++){
            discWorld.getPlayer_HASH().
                    put(index_of_player, new Player(index_of_player, player[index_of_player], colorArray[index_of_player], index_of_player, DEFAULT_PLAYER_PERSONALITYCARD));
        
            // create building piece
            for(int count = 0; count < NUM_OF_BUILDING_PIECE_SET; count++){
                BuildingPiece buildingPiece = new BuildingPiece(index_of_player*BUILDING_PIECE_ID_DIFF+count, index_of_player, DEFAULT_PIECE_AREA_NUMBER, colorArray[index_of_player]);
                discWorld.getBuildingPiece_HASH().put(buildingPiece.getId(), buildingPiece);
                
                // associate building with player
                discWorld.getPlayer_HASH().get(index_of_player).getBuildingList().add(buildingPiece.getId());
                
            }
            // create minion Piece
            for(int count = 0; count < NUM_OF_MINION_PIECE_SET; count++){
                MinionPiece minionPiece = new MinionPiece(index_of_player*MINION_PIECE_ID_DIFF+count, index_of_player, DEFAULT_PIECE_AREA_NUMBER, colorArray[index_of_player]);
                discWorld.getMinionPiece_HASH().put(minionPiece.getId(), minionPiece);
                
                // put one minion to 3 area
                if(count == 0){
                    discWorld.getMinionPiece_HASH().get(minionPiece.getId()).setAreaNumber(THE_SHADOW_AREA_NUM);
                    minionPiece.setAreaNumber(THE_SHADOW_AREA_NUM);
                    // associate minion with area
                    discWorld.getArea_HASH().get(THE_SHADOW_AREA_NUM).getMinionList().add(minionPiece.getId());
                } else if(count == 1){
                    discWorld.getMinionPiece_HASH().get(minionPiece.getId()).setAreaNumber(THE_SCOURS_AREA_NUM);
                    minionPiece.setAreaNumber(THE_SCOURS_AREA_NUM);
                    // associate minion with area
                    discWorld.getArea_HASH().get(THE_SCOURS_AREA_NUM).getMinionList().add(minionPiece.getId());
                } else if(count == 2){
                    discWorld.getMinionPiece_HASH().get(minionPiece.getId()).setAreaNumber(THE_DOLLY_SISTERS_AREA_NUM);
                    minionPiece.setAreaNumber(THE_DOLLY_SISTERS_AREA_NUM);
                    // associate minion with area
                    discWorld.getArea_HASH().get(THE_DOLLY_SISTERS_AREA_NUM).getMinionList().add(minionPiece.getId());
                }
                
                
                
                // associate minion with player
                discWorld.getPlayer_HASH().get(index_of_player).getMinionList().add(minionPiece.getId());
                
            }
            
            // give each player 2 gold coin
            for(Coin coin : discWorld.getCoin_HASH().values()){
                if(coin.getType() == Coin.GOLD && coin.getPlayerID() == DEFAULT_UNDEFINED_PLAYID){
                    discWorld.getPlayer_HASH().get(index_of_player).getCoinList().add(coin.getId());
                    coin.setPlayerID(index_of_player);
                    if(discWorld.getPlayer_HASH().get(index_of_player).getCoinList().size() == DEFAULT_NUM_GOLD_COIN_FOR_PLAYER) 
                        break;
                }
                
            }
            
            // give each player 5 green player card
            for(PlayerCard playerCard : discWorld.getPlayerCard_HASH().values()){
                if(playerCard.getType() == PlayerCard.GREEN && playerCard.getPlayerID() == DEFAULT_UNDEFINED_PLAYID){
                    discWorld.getPlayer_HASH().get(index_of_player).getPlayerCardList().add(playerCard.getId());
                    playerCard.setPlayerID(index_of_player);
                    if(discWorld.getPlayer_HASH().get(index_of_player).getPlayerCardList().size() == DEFAULT_NUM_PLAYER_CARD_FOR_PLAYER)
                        break;
                }
            }
            
        }
        
        // give each player a Personality Card
        Random rand = new Random();
        Set<Integer> generatedRandomNum = new LinkedHashSet<Integer>();
        while (generatedRandomNum.size() < numPlayer) {
            int randomNumber = rand.nextInt(NUM_PERSONALITY_CARD) + 1;
            generatedRandomNum.add(randomNumber);
        }
        Queue<Integer> generatedRandomList = new LinkedList<Integer>();
        for(Integer integer : generatedRandomNum){
            generatedRandomList.add(integer);
        }
        for(Player temp_player : discWorld.getPlayer_HASH().values()){
            temp_player.setPersonalityCardID(generatedRandomList.poll());
            discWorld.getPersonalityCard_HASH().get(temp_player.getPersonalityCardID()).setPlayerID(temp_player.getID());
        }
        
        
        for(int count = 0; count < NUM_DEMON_PIECE; count++){
            DemonPiece demonPiece = new DemonPiece(count, DEFAULT_PIECE_AREA_NUMBER);
            discWorld.getDemonPiece_HASH().put(count, demonPiece);
        }
        
        for(int count = 0; count < NUM_TROLL_PIECE; count++){
            TrollPiece trollPiece = new TrollPiece(count, DEFAULT_PIECE_AREA_NUMBER);
            discWorld.getTrollPiece_HASH().put(count, trollPiece);
        }
        
        for(int count = 0; count < NUM_TROUBLE_MARKER; count++){
            TroubleMarker troubleMarker = new TroubleMarker(count, DEFAULT_PIECE_AREA_NUMBER);
            discWorld.getTroubleMarker_HASH().put(count, troubleMarker);
            
            // put one trouble marker to 3 area
                if(count == 0){
                    discWorld.getTroubleMarker_HASH().get(troubleMarker.getId()).setAreaNumber(THE_SHADOW_AREA_NUM);
                    troubleMarker.setAreaNumber(THE_SHADOW_AREA_NUM);
                    discWorld.getArea_HASH().get(THE_SHADOW_AREA_NUM).setTroubleMarkerNum(troubleMarker.getId());
                    
                } else if(count == 1){
                    discWorld.getTroubleMarker_HASH().get(troubleMarker.getId()).setAreaNumber(THE_SCOURS_AREA_NUM);
                    troubleMarker.setAreaNumber(THE_SCOURS_AREA_NUM);
                    discWorld.getArea_HASH().get(THE_SCOURS_AREA_NUM).setTroubleMarkerNum(troubleMarker.getId());
                    
                } else if(count == 2){
                    discWorld.getTroubleMarker_HASH().get(troubleMarker.getId()).setAreaNumber(THE_DOLLY_SISTERS_AREA_NUM);
                    troubleMarker.setAreaNumber(THE_DOLLY_SISTERS_AREA_NUM);
                    discWorld.getArea_HASH().get(THE_DOLLY_SISTERS_AREA_NUM).setTroubleMarkerNum(troubleMarker.getId());
                    
                }
        }
        
        
        
        
        
    }

    /**
     *Function to get the player number.
     * @return player number.
     */
    public int getNumPlayer() {
        return numPlayer;
    }

    /**
     *Function to set player number.
     * @param numPlayer
     */
    public void setNumPlayer(int numPlayer) {
        this.numPlayer = numPlayer;
    }

    /**
     *Function to get the game data.
     * @return  DiscWorld
     */
    public DiscWorld getDiscWorld() {
        return discWorld;
    }

    /**
     *Function to set the game data.
     * @param discWorld
     */
    public void setDiscWorld(DiscWorld discWorld) {
        this.discWorld = discWorld;
    }
    
    
}
