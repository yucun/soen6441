
package Model.card;

/**
 *
 * @author yucunli
 */
public class PlayerCard extends Card{

    /**
     *Player card name.
     */
    protected String name;

    /**
     *Player card action.
     */
    protected String action;

    /**
     *Player card type.
     */
    protected String type;

    /**
     *Player card color: Green.
     */
    public static final String GREEN = "Green";

    /**
     *Player card colot: Brown.
     */
    public static final String Brown = "Brown";
    
    /**
     *Constructor, set the player card ID, name, action and player ID.
     * @param id
     * @param name
     * @param action
     * @param playerID
     */
    public PlayerCard(int id, String name, String action, int playerID){
        super(id, playerID);
        this.name = name;
        this.action = action;
    }

    /**
     *Function to get the player card name.
     * @return player card name.
     */
    public String getName() {
        return name;
    }

    /**
     *Function to set the player card name.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *Function to get the player card action.
     * @return player card action.
     */
    public String getAction() {
        return action;
    }

    /**
     *Function to set the player card action.
     * @param action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     *Function to get the player card type.
     * @return player card type.
     */
    public String getType() {
        return type;
    }

    /**
     *Function to set the player card type.
     * @param type
     */
    public void setType(String type) {
        this.type = type;
    }
    
}
