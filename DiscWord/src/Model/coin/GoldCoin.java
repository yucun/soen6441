
package Model.coin;

/**
 *
 * @author yucunli
 */
public class GoldCoin extends Coin{

    /**
     *Constructor, set the gold coin ID, player ID.
     * @param id
     * @param playerID
     */
    public GoldCoin(int id, int playerID) {
        super(id, playerID);
        this.type = "Gold";
    }
    
}
