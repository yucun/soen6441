
package Model.coin;

/**
 *
 * @author yucunli
 */
public class SilverCoin extends Coin{

    /**
     *Constructor, set the silver coin ID,name.
     * @param id
     * @param playerID
     */
    public SilverCoin(int id, int playerID) {
        super(id, playerID);
        this.type = "Silver";
    }
    
}
