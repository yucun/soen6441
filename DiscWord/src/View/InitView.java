
package View;

import Controller.BasicFunction;
import Controller.Utils;
import Model.piece.DemonPiece;
import Model.piece.TrollPiece;
import Model.piece.TroubleMarker;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author ADMINISTRATOR
 */
public class InitView {
    BasicFunction basicFunction;

    /**
     *Constructor, set the basic function.
     * @param basicFunction
     */
    public InitView(BasicFunction basicFunction) {
        this.basicFunction = basicFunction;
    }
    
    
    // all 12 areas

    /**
     *Function to initial the 12 areas information.
     * @param jTextArea
     * @param areaNumber
     */
        public void initArea(JTextArea jTextArea, int areaNumber){
        int buildingNumber = basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getBuildingNum();
        
        jTextArea.setText("Area name: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getNamePlate()+"\n"
                            +"Area number: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getNumber()+"\n"
                            +"Area building cost: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getBuildingCost()+"\n"
                            +"Adjacent Area: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getAdjacentArea().toString()+"\n"
                            +"Area building number: "+(buildingNumber==BasicFunction.DEFAULT_PIECE_AREA_NUMBER?"Empty":basicFunction.getDiscWorld().getBuildingPiece_HASH().get(buildingNumber).getColor())+"\n"
                            +"Area minion: "+Utils.minionListToColorString(basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getMinionList(), basicFunction.getDiscWorld().getMinionPiece_HASH())+"\n"
                            +"Area TroubleMarker: "+(basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getTroubleMarkerNum()==BasicFunction.DEFAULT_PIECE_AREA_NUMBER?"False":"True")+"\n"
                            +"Area Troll number: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getTrollList().size()+"\n"
                            +"Area Demon number: "+basicFunction.getDiscWorld().getArea_HASH().get(areaNumber).getDemonList().toString()+"\n");
    }

    /**
     *Function to initial the player board information.
     * @param jTextPlayer
     * @param playerNumber
     */
    public void initPlayer(JTextArea jTextPlayer, int playerNumber){
        try{
        int playerHoldingMinionCount = 0;
        for(Integer integer : basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getMinionList()){
            if(basicFunction.getDiscWorld().getMinionPiece_HASH().get(integer).getAreaNumber() == BasicFunction.DEFAULT_PIECE_AREA_NUMBER){
                playerHoldingMinionCount += 1;
            }
        }
        int playerHoldingBuildingCount = 0;
        for(Integer integer : basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getBuildingList()){
            if(basicFunction.getDiscWorld().getBuildingPiece_HASH().get(integer).getAreaNumber() == BasicFunction.DEFAULT_PIECE_AREA_NUMBER){
                playerHoldingBuildingCount += 1;
            }
        }
        int personalityCardID  = basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getPersonalityCardID();
        String playerHoldingArea = "";
        for(Integer integer : basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getCityAreaCardList()){
            playerHoldingArea += basicFunction.getDiscWorld().getCityAreaCard_HASH().get(integer).getName()+" ";
        }
        
        
        jTextPlayer.setText("Player name:"+basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getName()+"\n"
                            +"Player ID: "+basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getID()+"\n"
                            +"Player Color: "+basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getColor()+"\n"
                            +"Player Money#: "+basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getTotalCoinAmount(basicFunction.getDiscWorld().getCoin_HASH())+"\n"
                            +"Player Minion#: "+playerHoldingMinionCount+"\n"
                            +"Player Buildings#: "+playerHoldingBuildingCount+"\n"
                            +"Player Control Area: "+playerHoldingArea+"\n"
                            +"Personality Card: "+basicFunction.getDiscWorld().getPersonalityCard_HASH().get(personalityCardID).getName()+"\n"
                            +"Player Card: "+basicFunction.getDiscWorld().getPlayer_HASH().get(playerNumber).getPlayerCardList().toString()+"\n");
        }catch(NullPointerException e){
            jTextPlayer.setText("");
        }
    }
    //additional information

    /**
     *Function to initial all the pieces on the map.
     * @param demonNumber
     * @param trollNumber
     * @param troubleNumber
     * @param bankMoney
     * @param currentPlayer
     */
        public void initMapInfo(JLabel demonNumber,JLabel trollNumber,JLabel troubleNumber,JLabel bankMoney,JLabel currentPlayer){
        int demonCount = 0;
        for(DemonPiece demon : basicFunction.getDiscWorld().getDemonPiece_HASH().values()){
            if(demon.getAreaNumber() == BasicFunction.DEFAULT_PIECE_AREA_NUMBER){
                demonCount++;
            }
        }
        
        int trollCount = 0;
        for(TrollPiece troll : basicFunction.getDiscWorld().getTrollPiece_HASH().values()){
            if(troll.getAreaNumber() == BasicFunction.DEFAULT_PIECE_AREA_NUMBER){
                trollCount++;
            }
        }
        
        int troubleCount = 0;
        for(TroubleMarker troubleMarker : basicFunction.getDiscWorld().getTroubleMarker_HASH().values()){
            if(troubleMarker.getAreaNumber() == BasicFunction.DEFAULT_PIECE_AREA_NUMBER){
                troubleCount++;
            }
        }
        
        demonNumber.setText(demonCount+"");
        trollNumber.setText(trollCount+"");
        troubleNumber.setText(troubleCount+"");
        bankMoney.setText(basicFunction.getDiscWorld().getTotalCoinAmount()+"");
        currentPlayer.setText(basicFunction.getDiscWorld().getPlayer_HASH().get(basicFunction.getDiscWorld().getCur_Player()).getName());
    }

}
